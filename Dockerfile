# get shiny server and R from the rocker project
ARG R_VERSION=4.1.0
ARG IMAGE_REGISTRY
ARG DOCKER_IMAGE=rocker/r-ver:$R_VERSION
FROM $IMAGE_REGISTRY$DOCKER_IMAGE AS base

# system libraries
# Try to only install system libraries you actually need
# Package Manager is a good resource to help discover system deps
RUN apt-get update \
    && apt-get install -y --no-install-recommends \
        # shiny dependencies
        libssl-dev libcurl4-gnutls-dev libxml2-dev \
        # application dependencies
        libudunits2-dev libproj-dev libgdal-dev \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

FROM base AS development
# create app directory
RUN mkdir /build
WORKDIR /build
# copy DESCRIPTION fisrt and install dependencies
# usefull to speed up the image's build
COPY DESCRIPTION .
RUN R -e 'install.packages("remotes"); remotes::install_cran("attachment"); attachment::install_from_description()'

# copy application and install package
RUN mkdir /app
WORKDIR /app
COPY . .
RUN R -e 'remotes::install_local(upgrade="never")'

FROM development AS test

RUN echo "exécution des tests !"

FROM development AS runtime

# Init user with default values
ARG USERNAME="shiny"
ARG USERID="10001"
ARG GROUPNAME="shiny"
ARG GROUPID="10001"

# Create execution user/group
RUN echo "Creatin group ${GROUPNAME}:${GROUPID}"
RUN groupadd -g ${GROUPID} ${GROUPNAME}
RUN echo "Creatin user ${USERNAME}:${USERID}"
RUN useradd -g ${GROUPNAME} -d /app -u ${USERID} ${USERNAME}

# run application as non root
EXPOSE 3838
CMD  ["R", "-e", "options('shiny.port'=3838,shiny.host='0.0.0.0','shiny.fullstacktrace'=TRUE);shiny::runApp('./shiny-app/app.R')"]