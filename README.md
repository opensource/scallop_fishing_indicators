# SCALLOP FISHING INDICATORS

Indicateurs de sensibilité, d'exposition, de capacité d'adaptation et de vulnérabilité des navires de pêche de coquilles Saint-Jacques en Manche-Est face aux risques des efflorescences algales nuisibles (HABs).

Les fichiers :
- DESCRIPTION : installation automatique des paquets
- Dockerfile : image docker au sein de laquelle s'execute l'application
- README.md : documentation

Les répertoires :
- shiny-app : Code source de l'application
- data-raw : Répertoire contenant les données

## Production de l'image docker

Créer un nouveau tag nouvelle version en respectant la nomenclature semver.

Cela déclenche un pipeline pour produire une image docker qui sera publiée sur le registre docker du projet : <https://gitlab.ifremer.fr/opensource/scallop_fishing_indicators/container_registry>

## Récupérer les sources

- Cloner le projet Gitlab 

`git clone https://gitlab.ifremer.fr/opensource/scallop_fishing_indicators.git`

## Mettre à jour les données 
TODO ? 

## Exécuter l'application
### Localement sous Windows

#### Prérequis
- Installer Docker

<https://docs.docker.com/desktop/install/windows-install/>

- Installer un sous-système Linux (Ubuntu par exemple)

<https://ubuntu.com/tutorials/install-ubuntu-on-wsl2-on-windows-10#1-overview>

#### Image locale

- 1. Initialiser/lancer Docker (ouvrir docker Desktop par exemple)
- 2. Ouvrir le terminal Linux
- 3. Dans celui-ci, monter le projet Windows->Linux :
`cd /mnt/c/Users/[USER]/[PATH_TO_YOUR_LOCAL_APPLICATION]`
- 5. Produire l'image :
`docker build -t scallop_fishing_indicators .`
- 6. Exécuter l'image : 
`docker run --rm -p 3838:3838 scallop_fishing_indicators `
- 7. Accéder à l'application via un navigateur : <http://localhost:3838/>

### Localement sous Unix

#### Prérequis
- Installer Docker : <https://docs.docker.com/engine/install/>

#### Image locale
- 1. Produire et exécuter l'image :
```bash
cd [PATH_TO_YOUR_LOCAL_APPLICATION]
docker build -t scallop_fishing_indicators .
docker run --rm -p 3838:3838 scallop_fishing_indicators 
```

- 2. Accéder à l'application via un navigateur : <http://localhost:3838/>

#### Dernière image du registre docker du projet
```bash
echo _FzSQvjeVbpxUgim8e2y | docker login gitlab-registry.ifremer.fr --password-stdin -u gitlab+deploy-token-ro
docker pull gitlab-registry.ifremer.fr/opensource/scallop_fishing_indicators
docker run --rm -p 3838:3838 gitlab-registry.ifremer.fr/opensource/scallop_fishing_indicators 
docker logout gitlab-registry.ifremer.fr
```